#include <stdio.h>
#include <math.h>
#include <stdbool.h>

bool is_integer(float num)
{
	// An off-by-one error caused by float rounding will be evident in the output
	// #cheeky
	if (floorf(num) == num) {
		return true;
	}

	return false;
}

int main()
{
	/* This uses the rearrangement of the problem:
	 * Given `a + b + c = 1000`
	 * and `a^2 + b^2 = c^2`:
	 * a + b + sqrt(a^2 + b^2) = 1000
	 * sqrt(a^2 + b^2) = 1000 - a - b
	 * a^2 + b^2 = (1000 - a - b)^2
	 * a^2 + b^2 = a^2 + b^2 + 1000000 - 2ab - 2000a - 2000b
	 * 0 = 1000000 - 2ab - 2000a - 2000b
	 * 0 = 1000000 - 2000a - b(2a - 2000)
	 * b = (1000000 - 2000a)/(2a - 2000)
	 * b = (500000 - 1000a)/(a - 1000)
	 */

	float a_temp = 0.0f;
	float b_temp = 0.0f;

	// Increment a and exit the loop when an integer b is found
	do {
		a_temp += 1;

		// b will be negative because a < 1000
		b_temp = (500000 - 1000 * a_temp) / fabsf((a_temp - 1000));
	}
	while (!is_integer(b_temp));

	// Pretty-print the results
	int a = (int)a_temp, b = (int)b_temp, c = 1000 - (a + b);

	printf("a = %d, b = %d, c = %d\nProduct: %d\n", a, b, c, a * b * c);
	return 0;
}

