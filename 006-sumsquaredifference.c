#include <stdio.h>

unsigned long long sum_to(unsigned long long n) {
	return n * (n + 1) / 2;
}

unsigned long long sum_of_squares_to(unsigned long long n) {
	unsigned long long result = 0;
	
	for (int i = 1; i < n + 1; i++) {
		result += i * i;
	}

	return result;
}

int main()
{
	unsigned long long sum_of_squares = sum_of_squares_to(100);
	unsigned long long sum_to_100 = sum_to(100);
	unsigned long long square_of_sum = sum_to_100 * sum_to_100;

	printf("%llu\n", square_of_sum - sum_of_squares);
	return 0;
}

