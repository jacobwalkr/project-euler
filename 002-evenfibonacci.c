#include <stdio.h>

int main()
{
	int a = 1, b = 2, total;

	while (b < 4e6) {
		if (b % 2 == 0) {
			total += b;
		}

		int next = a + b;
		a = b;
		b = next;
	}

	printf("%d\n", total);
	return 0;
}

