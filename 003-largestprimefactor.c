#include <stdio.h>

#define ulong unsigned long long

// Gotta deal with some big numbers, yo.
// Using `ulong int` for portability, even though `unsigned long
// int` is big enough on my machine.
ulong largest_prime_factor(ulong number)
{
	ulong lpf = 2;

	while (number > lpf) {
		if (number % lpf == 0) {
			number /= lpf;
			lpf = 2;
		}
		else {
			lpf++;
		}
	}

	return lpf;
}

int main()
{
	ulong number = 600851475143; // could use scanf("%llu", &p)
	
	printf("%llu\n", (ulong)largest_prime_factor(number));
	return 0;
}

