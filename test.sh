#!/bin/bash
file=$1
file_noext=${file::-2}

echo "Building..."
gcc "$file" -Wall -Werror -std=c11 -o bin/"$file_noext"

if [ $? -eq 0 ]; then
    echo "Running..."
    time bin/"$file_noext"
fi

