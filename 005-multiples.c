#include <stdio.h>
#include <stdbool.h>

bool is_fully_divisible(int number, int this_divisor, int min_divisor)
{
	if (number % this_divisor == 0) {
		if (this_divisor == min_divisor) {
			return true;
		}
		else {
			return is_fully_divisible(number, --this_divisor, min_divisor);
		}
	}
	
	// Fallen through:
	return false;
}

int main()
{
	for (int i = 20;; i += 20) {
		if (is_fully_divisible(i, 19, 11)) {
			printf("Found it: %d!\n", i);
			break;
		}
	}

	return 0;
}

