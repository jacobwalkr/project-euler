#include <stdio.h>
#include <stdbool.h>

bool is_palindrome(int n)
{
	int reverse = 0, temp = n;

	while (temp != 0) {
		reverse = reverse * 10 + temp % 10;
		temp /= 10;
	}

	return reverse == n;
}

int main()
{
	int largest = 0, largest_a = 0, largest_b = 0, product;
	
	for (int a = 999; a > 100; a--) {
		for (int b = 999; b > 100; b--) {
			product = a * b;

			if (is_palindrome(product) && product > largest) {
				largest = product;
				largest_a = a;
				largest_b = b;
			}
		}
	}

	printf("Largest found: %d x %d = %d\n", largest_a, largest_b, largest);
	return 0;
}

