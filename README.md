Project Euler - My Solutions
============================

This repository stores my code for Project Euler problems. The code was compiled in `gcc` with options `-Wall -Werror -std=c11` (and, in cases where math.h was required, `-lm`) and tested on Ubuntu (version 15.10 or later).

Many of these will be far from perfect, but hopefully I'll be able to come back to them later on and improve them!

If you have yet to solve these problems and think you might, then have a go first! None of the actual answers are here, but the code is.

http://projecteuler.net/