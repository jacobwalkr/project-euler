#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	const int LIMIT = 2e6;
	bool *sieve = malloc(sizeof(bool) * LIMIT);
	//bool *sieve[LIMIT];

	// Fill sieve with trues so it's simpler to think about
	memset(sieve, true, sizeof(sieve[0]) * LIMIT);

	unsigned long long sum = 0; // Just in case!

	// Do the sieving
	for (int i = 2; i < LIMIT; i++) {
		if (sieve[i]) {
			// Houston, we have a prime
			sum += i;

			// Cross off the multiples of i
			for (int j = i + i; j <= LIMIT; j += i) {
				sieve[j] = false;
			}
		}
	}

	printf("Sum of all primes under %d: %llu\n", LIMIT, sum);

	free(sieve);
	return 0;
}

