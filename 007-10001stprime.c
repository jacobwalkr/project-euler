#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

int main()
{
	// Will try increasing limits until I find the 10001st prime.
	// I'm doing this blind, so I'm not sure where I'll find it!
	const int LIMIT = 1e6;
	const int MAX_PRIME_FACTOR = ceil(sqrt((double)LIMIT));
	bool *sieve = malloc(sizeof(bool) * LIMIT);

	// Fill the array with trues, so it's easier to think about
	for (int i = 0; i < LIMIT; i++) {
		sieve[i] = true;
	}

	// Do the sieving
	for (int i = 2; i < MAX_PRIME_FACTOR; i++) {
		if (sieve[i]) {
			// Cross off the multiples of i
			for (unsigned int j = i + i; j <= LIMIT; j += i) {
				sieve[j] = false;
			}
		}
	}

	// Count up the primes!
	int counter = 0;

	// Skip 0 and 1
	for (int i = 2; i < LIMIT; i++) {
		//printf("Check sieve[%d]: %s\n", i, sieve[i] ? "prime" : "not prime");
		if (sieve[i]) {
			counter++;

			if (counter == 10001) {
				printf("%d\n", i);
				break;
			}
		}
	}
	
	free(sieve);
	
	// Check whether we've found the 10001st prime
	if (counter != 10001) {
		printf("Got to %d: 10001st prime not found :(\n", counter);
		return 1;
	}

	return 0;
}

